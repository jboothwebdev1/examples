package main

import (
	"net/http"
    "strconv"

	"github.com/labstack/echo/v4"
)

type (
  todo struct {
    ID int `json:"id"`
    Name string `json:"name"`
    IsComplete bool `json:"isComplete"`
  } 
)

var (
  todos = map[int]*todo{}
  seq = 1
)

func createTodo(c echo.Context) error {
  u := &todo {
    ID: seq,
  }

  if err := c.Bind(u); err != nil {
    return err
  }
 
  todos[u.ID] = u
  seq++
  return c.JSON(http.StatusCreated, u)
}

func deleteTodo(c echo.Context) error {
  id, _ := strconv.Atoi(c.Param("id"))
  delete(todos, id)
  return c.NoContent(http.StatusNoContent)
}

func getAllTodo(c echo.Context) error {
  return c.JSON(http.StatusOK, todos)
}

func getTodo(c echo.Context) error {
  id, _ := strconv.Atoi(c.Param("id"))

  return c.JSON(http.StatusOK, todos[id])
}

func updateTodo(c echo.Context) error {
  u := new(todo)
  if err := c.Bind(u); err != nil {
    return err
  }
  id, _ := strconv.Atoi(c.Param("id"))
  todos[id].Name = u.Name
  todos[id].IsComplete = u.IsComplete
  return c.JSON(http.StatusOK, todos[id])
}

func test(c echo.Context) error {
   return c.String(http.StatusOK, "Hello World!")
}

func main (){
  e := echo.New()
  e.GET("/test", test)
  e.GET("/todo/:id", getTodo)
  e.GET("/todo", getAllTodo)
  e.DELETE("/todo/:id", deleteTodo)
  e.POST("/todo", createTodo)

  e.Logger.Fatal(e.Start(":9090"))
}
