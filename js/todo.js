'use strict'

let todos = []
let seq = 1


export function getAllTodos(req, res){
  res.status(200).json(todos)
}

export function getTodo(req, res){
  let id = req.params.id
  let todo = todos.forEach((todo) => {
    if(todo.id === id) {
       return todo
    }
  })

  if (todo) {
    res.status(200).json(todo)
  } else {
    res.status(404).send("Todo not found.")
  }
}

export function createTodo(req, res){
  let todo = req.body.todo
  todo.id = seq
  seq++
  todos.push(todo)
  res.status(200).json(todo)
}

export function updateTodo(req, res){
  let id = req.params.id

  let todo = todos.forEach((todo) => {
    if (todo.id == id){
      return todo
    }
  })
  todo =  {
    id: id,
    name: req.body.todo.name,
    isComplete: req.body.todo.isComplete,
  }
  res.status(200).json(todo)
}

export function deleteTodo(){
  let id = req.params.id

  let todosFiltered = todos.filter((todo) => {
    if (todo.id !== id){
      return todo
    }
  }) 

  todos = todosFiltered
  res.status(200)
} 
