'use strict'

const express = require('express')
const todos = require('./todo')

const app = express()
const port = 7070

app.get('/test', (req, res) => {
  res.send('Hello World!')
})

// Todos
app.get('/todo', todos.getAllTodos)
app.post('/todo', todos.createTodo)
app.put('/todo/:id', todos.updateTodo)

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
