use actix_web::{get,post,put,delete, Responder, HttpResponse, HttpRequest, web};
use serde::{Deserialize, Serialize};

use crate::AppState;

#[derive(Debug, Deserialize, Serialize, PartialEq, Clone)]
pub struct Todo {
    id: u32,
    name: String,
    is_complete: bool
}

#[get("/todo")]
async fn get_all_todos(todos: web::Data<AppState>) -> impl Responder {
    let items = &todos.todos;
    HttpResponse::Ok().json(items)
}

#[get("/todo/{id}")]
async fn get_todo(todos: web::Data<AppState>, path: web::Path<u32>) -> impl Responder {
    let id = path.into_inner();
    let todo = todos.todos
                    .lock()
                    .unwrap()
                    .clone();

    let todo = todo.iter().find(|todo| todo.id == id);

    match todo {
        Some(todo) => HttpResponse::Ok().json(todo),
        None => HttpResponse::NotFound().body("Todo with that id, Not found.")
    }
}

#[post("/todo")]
async fn create_todo(todos: web::Data<AppState>, body: web::Json<Todo>) -> impl Responder {
    let todo = Todo {
        id: todos.series,
        name: body.name.clone(),
        is_complete: body.is_complete
    };
    
    todos.todos.lock().unwrap().push(todo.clone());

    HttpResponse::Ok().json(todo)
}

#[put("/todo/")]
async fn update_todo(todos: web::Data<AppState>, body: web::Json<Todo>) -> impl Responder {
    let mut todos = todos.todos.lock().unwrap();

    let current_todo = todos
        .drain_filter( |todo| todo.id == body.id).take(1).next();

    match current_todo {
        Some(mut todo) => {
            todo.name = body.name.clone();
            todo.is_complete = body.is_complete;
            todos.push(todo.clone()); 
            HttpResponse::Ok().json(todo)
        }
        None => HttpResponse::NotFound().body("Todo not found.")
    }

}

#[delete("/todo/")]
async fn detele_todo(todos: web::Data<AppState>, body: web::Json<Todo>) -> impl Responder {
    let mut todos = todos.todos.lock().unwrap();

    let current_todo = todos
        .drain_filter( |todo| todo.id == body.id).take(1).next();

    match current_todo {
        Some(_) => HttpResponse::Accepted().body("Todo deleted."), 
        None => HttpResponse::NotFound().body("Todo not found.")
    }
}

pub fn config(cfg: &mut web::ServiceConfig){
    cfg.service(get_todo);
    cfg.service(get_all_todos);
    cfg.service(create_todo);
    cfg.service(update_todo);
    cfg.service(detele_todo);
} 

