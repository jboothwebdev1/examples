#![feature(drain_filter)]
mod todo;
use std::sync::Mutex;

use actix_web::{ App, get, HttpServer, Responder, HttpResponse, web};
use todo::{Todo, config};
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
struct AppState {
    series: u32,
    todos: Mutex<Vec<Todo>>
}

#[get("/test")]
async fn test() -> impl Responder {
    HttpResponse::Ok().body("Hello World!")
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let todos = web::Data::new(AppState {
        series: 1,
        todos:  Mutex::new(vec![]),
    });

    HttpServer::new(move || {
        App::new()
            .app_data(todos.clone())
            .configure(config)
            .service(test)
    })
    .bind(("0.0.0.0", 8080))?
    .run()
    .await
}
